#include "Max30205Sensor.h"

Max30205Sensor::Max30205Sensor(TwoWire &wirePort){
	_i2cPort = &wirePort;
}

float Max30205Sensor::getTemperature(void){
	uint8_t readRaw[2] = {0};
    I2CReadBytes(MAX30205_ADDRESS,MAX30205_TEMPERATURE, &readRaw[0] ,2); // read two bytes
	int16_t raw = readRaw[0] << 8 | readRaw[1];  //combine two bytes
    temperature = raw  * 0.00390625;     // convert to temperature
	return  temperature;
}

int16_t Max30205Sensor::getRawTemperature(void){
	uint8_t readRaw[2] = {0};
    I2CReadBytes(MAX30205_ADDRESS,MAX30205_TEMPERATURE, &readRaw[0] ,2); // read two bytes
	int16_t raw = readRaw[0] << 8 | readRaw[1];  //combine two bytes
	return  raw;	
}

void Max30205Sensor::shutdown(void){
  uint8_t reg = I2CReadByte(MAX30205_ADDRESS, MAX30205_CONFIGURATION);  // Get the current register
  I2CWriteByte(MAX30205_ADDRESS, MAX30205_CONFIGURATION, reg | 0x80);
}

void Max30205Sensor::begin(void){
  I2CWriteByte(MAX30205_ADDRESS, MAX30205_CONFIGURATION, 0x00); //mode config
  I2CWriteByte(MAX30205_ADDRESS, MAX30205_THYST , 		 0x00); // set threshold
  I2CWriteByte(MAX30205_ADDRESS, MAX30205_TOS, 			 0x00); //
}

// _i2cPort->h read and write protocols
void Max30205Sensor::I2CWriteByte(uint8_t address, uint8_t subAddress, uint8_t data)
{
	_i2cPort->beginTransmission(address);  // Initialize the Tx buffer
	_i2cPort->write(subAddress);           // Put slave register address in Tx buffer
	_i2cPort->write(data);                 // Put data in Tx buffer
	_i2cPort->endTransmission();           // Send the Tx buffer
}

uint8_t Max30205Sensor::I2CReadByte(uint8_t address, uint8_t subAddress)
{
	uint8_t data; // `data` will store the register data
	_i2cPort->beginTransmission(address);
	_i2cPort->write(subAddress);
	_i2cPort->endTransmission(false);
	_i2cPort->requestFrom(address, (uint8_t) 1);
	data = _i2cPort->read();
	return data;
}

void Max30205Sensor::I2CReadBytes(uint8_t address, uint8_t subAddress, uint8_t * dest, uint8_t count)
{
	_i2cPort->beginTransmission(address);   // Initialize the Tx buffer
	// Next send the register to be read. OR with 0x80 to indicate multi-read.
	_i2cPort->write(subAddress);
	_i2cPort->endTransmission(false);
	uint8_t i = 0;
	_i2cPort->requestFrom(address, count);  // Read bytes from slave register address
	while (_i2cPort->available())
	{
		dest[i++] = _i2cPort->read();
	}
}
