/*
 * MoSimPa ESP32 firmware
 * Copyright 2020 Lisandro Damián Nicanor Pérez MEyer <perezmeyer@gmail.com>
 * License: to be defined, but possibly MIT/Apache 2.0.
 */

/**
 * \todo We are sending the password in clear.
 */
#include "printf.h"
#include <Ticker.h>
#include <Wire.h>
#include "Max30101Sensor.h"
#include "Max30205Sensor.h"
/* ------------------------------------------------------------- */
#define PERIOD_SEND_INFO    60  // seconds
#define LED_BUILTIN         2
#define I2C_SDA_MAX30101    33
#define I2C_SCL_MAX30101    32
#define I2C_SDA_MAX30205    19
#define I2C_SCL_MAX30205    18

TwoWire I2CMax30101 = TwoWire(0);
TwoWire I2CMax30205 = TwoWire(0);

Max30101Sensor max30101Sensor(I2CMax30101);
Max30205Sensor tempSensor(I2CMax30205);

Ticker tickerLed;
Ticker tickerMax30101;
/* ------------------------------------------------------------- */
void sensors_process(void);
/* ------------------------------------------------------------- */
void blinkLed(){
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
}
void Max30101Sensor_update(void){
  max30101Sensor.update();
}
/* ------------------------------------------------------------- */
/**
 * \brief Inicializacion
 */
void setup() {  

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  tickerLed.attach(0.25, blinkLed);
  Serial.begin(115200);

  I2CMax30101.begin(I2C_SDA_MAX30101, I2C_SCL_MAX30101, I2C_SPEED_STANDARD);

  tempSensor.begin();
  max30101Sensor.begin();
  tickerMax30101.attach(0.4, Max30101Sensor_update);
}
/**
 * \brief bucle infinito
 */
void loop(){
  int32_t spo2, heartRate;
  int16_t temp;
  
  delay(1000);
  max30101Sensor.SPO2AndHeartRateGet(&spo2, &heartRate);
  printfn("SPO2: %d", spo2);
  printfn("HR: %d", heartRate);
  temp = tempSensor.getRawTemperature();
  printfn("temp: %u, 0x%04X", temp, temp);
}
/* ------------------------------------------------------------- */
/* ------------------------------------------------------------- */
/**
 * \brief
 */
void sensors_process(void){

}
