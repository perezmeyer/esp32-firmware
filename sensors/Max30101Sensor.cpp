#include "Max30101Sensor.h"

/**
 * \brief
 */
Max30101Sensor::Max30101Sensor(TwoWire &wirePort){
    _i2cPort = &wirePort;
}
/**
 * \brief
 */
void Max30101Sensor::begin(){
    uint16_t k;    
    
    // Initialize sensor MAX30101
    if (!_max30101.begin(*_i2cPort)) 
    {
        printfn("MAX30101 was not found. Please check wiring/power.");
        while (1);
    }

    //Configure sensor with these settings
    _max30101.setup(LED_BRIGHTNESS, SAMPLE_AVERAGE, LED_MODE, SAMPLE_RATE, PULSE_WIDTH, ADC_RANGE);

    for (k = 0 ; k < SAMPLE_RATE ; k++) {
        while (_max30101.available() == false) //do we have new data?
        _max30101.check(); //Check the sensor for new data

        _redBuffer[k] = _max30101.getRed();
        _irBuffer[k] = _max30101.getIR();
    }
}
/**
 * \brief Se actualiza los buffres... Deberia ejecutarse cada 40ms (25sps)
 */
void Max30101Sensor::update(){

    if(_max30101.available()){
        _max30101.check(); 
        memcpy(&_redBuffer[0], &_redBuffer[1], (SAMPLE_RATE - 1)*4);
        memcpy(&_irBuffer[0], &_irBuffer[1], (SAMPLE_RATE - 1)*4);

    }
}
/**
 * \brief
 */
void Max30101Sensor::SPO2AndHeartRateGet(int32_t *spo2, int32_t *heartRate){
    int32_t _spo2; //SPO2 value
    int8_t _validSPO2; //indicator to show if the SPO2 calculation is valid
    int32_t _heartRate; //heart rate value
    int8_t _validHeartRate; //indicator to show if the heart rate calculation is valid

    //calculate heart rate and SpO2 after first 100 samples (first 4 seconds of samples)
    maxim_heart_rate_and_oxygen_saturation(_irBuffer, SAMPLE_RATE, _redBuffer, &_spo2, 
        &_validSPO2, &_heartRate, &_validHeartRate);

    *heartRate = 0;
    *spo2 = 0;
    if(_validSPO2){
        *spo2 = _spo2;
    }
    if(_validHeartRate){
        *heartRate = _heartRate;
    }
}
