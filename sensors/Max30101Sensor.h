#ifndef MAX30101_SENSOR_H
#define MAX30101_SENSOR_H

#include <Wire.h>
#include "MAX30105.h"
#include "spo2_algorithm.h"
#include "printf.h"

#define MAX_BRIGHTNESS  255
#define SAMPLE_RATE     100     //Options: 50, 100, 200, 400, 800, 1000, 1600, 3200
#define LED_BRIGHTNESS  60      //Options: 0=Off to 255=50mA
#define SAMPLE_AVERAGE  4       //Options: 1, 2, 4, 8, 16, 32
#define LED_MODE        2       //Options: 1 = Red only, 2 = Red + IR, 3 = Red + IR + Green
#define PULSE_WIDTH     411     //Options: 69, 118, 215, 411
#define ADC_RANGE       4096    //Options: 2048, 4096, 8192, 16384

class Max30101Sensor
{
private:
    MAX30105 _max30101;
    TwoWire *_i2cPort;
    uint32_t _irBuffer[SAMPLE_RATE];     //infrared LED sensor data
    uint32_t _redBuffer[SAMPLE_RATE];    //red LED sensor data
public:
    Max30101Sensor(TwoWire &wirePort = Wire);
    void begin();
    void update();
    void SPO2AndHeartRateGet(int32_t *spo2, int32_t *heartRate);
};


#endif
