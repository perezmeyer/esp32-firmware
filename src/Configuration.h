/*
 * MoSimPa ESP32 firmware
 * Copyright 2020 
 * Lisandro Damián Nicanor Pérez MEyer <perezmeyer@gmail.com>
 * Casanova Alejandro <acasanova.mails@gmail.com>
 */ 

#ifndef CONFIGURATION_H
#define CONFIGURATION_H
#include <inttypes.h>
#include <Arduino.h>

/// 32 bytes (per standard) + null character
#define WIFI_SSID_SIZE 33
/// 32 bytes + null character... just because I like it :-)
#define WIFI_PASS_SIZE 33
/// An IPv4 address has at most 15 characters, one extra for the null character.
#define MQTT_BROKER_IP_SIZE 16


typedef struct{
  char flag;
  char wifissid[WIFI_SSID_SIZE];
  char wifipass[WIFI_PASS_SIZE];
  char mqttip[MQTT_BROKER_IP_SIZE];
  int  mqttport;
}Parameters;

class ConfigurationClass
{
protected:
  Parameters parameters;
public:
  ConfigurationClass();
  void begin(void);
  bool isSetting(void);
  char *getSSID(void);
  char *getPass(void);
  char *getMqttIP(void);
  int getMqttPort(void);
  void setSSID(String ssid);
  void setPass(String pass);
  void setMqttIP(String ip);
  void setMqttPort(int port);
  void commit(void);
};

extern ConfigurationClass Configuration;

#endif
