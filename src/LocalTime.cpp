#include "LocalTime.h"
#include <WiFi.h>
#include "printf.h"

const char* ntpServer = "pool.ntp.org";
const long  gmtOffset_sec = -10800;     // -3 x 60 x 60
const int   daylightOffset_sec = 3600;  // o 0
static void callback();

static Ticker tickerSeconds;  
static time_t timeSeconds = 0;

static void LocalTime_callback(){
  timeSeconds++;
}

void LocalTime_begin(void){
#if USE_EPOCH == 1
  struct tm timeinfo;
  while(WiFi.status() != WL_CONNECTED);
 
  printf("Starting NTP connection...");
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
  while(getLocalTime(&timeinfo) == false);  
#else
  timeSeconds = millis()/1000;
  tickerSeconds.attach(1.0, LocalTime_callback);
#endif  
}

time_t LocalTime_getSeconds(void){
#if USE_EPOCH == 1
  struct tm timeinfo;
  if(getLocalTime(&timeinfo)){
    time_t timeSinceEpoch = mktime(&timeinfo);
    return(timeSinceEpoch);
  }
#else  
  return timeSeconds;
#endif    
}

time_t LocalTime_getEpoch(void){
#if USE_EPOCH == 1
  struct tm timeinfo;
  if(WiFi.status() == WL_CONNECTED){
    configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
    if(getLocalTime(&timeinfo)){
      time_t timeSinceEpoch = mktime(&timeinfo);
      return(timeSinceEpoch);
    }
  }
#endif

  return 0;
}
