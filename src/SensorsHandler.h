/*
 * MoSimPa ESP32 firmware
 * Copyright 2020 
 * Lisandro Damián Nicanor Pérez MEyer <perezmeyer@gmail.com>
 * Casanova Alejandro <acasanova.mails@gmail.com>
 */ 

#ifndef SENSORS_HANDLER_H
#define SENSORS_HANDLER_H
#include <inttypes.h>
#include <Arduino.h>
#include "time.h"

#define SPO2_SENSOR_BUFFER_SIZE 10
typedef struct{
  time_t time;
  uint16_t spO2;
  uint16_t R;
}SPO2SensorData;

class SPO2Sensor
{
protected:
  SPO2SensorData buffer[SPO2_SENSOR_BUFFER_SIZE];
  uint8_t indexWrite;
  uint8_t indexRead;
  uint8_t count;
  void read(SPO2SensorData *data);
public:
  SPO2Sensor();
  void clear();
  bool load(SPO2SensorData *data);
  void flush();
  bool loadAndFlush(SPO2SensorData *data);
};
/* ------------------------------------------- */
#define BLOODP_SENSOR_BUFFER_SIZE 10
typedef struct{
  time_t time;
  uint8_t sys;
  uint8_t dia;
}BloodPSensorData;

class BloodPSensor
{
protected:
  BloodPSensorData buffer[BLOODP_SENSOR_BUFFER_SIZE];
  uint8_t indexWrite;
  uint8_t indexRead;
  uint8_t count;
  void read(BloodPSensorData *data);
public:
  BloodPSensor();
  void clear();
  bool load(BloodPSensorData *data);
  void flush();
  bool loadAndFlush(BloodPSensorData *data);
};
/* ------------------------------------------- */
#define HEARTR_SENSOR_BUFFER_SIZE 10
typedef struct{
  time_t time;
  uint16_t heartR;
  bool HR_AR;
}HeartRSensorData;

class HeartRSensor
{
protected:
  HeartRSensorData buffer[HEARTR_SENSOR_BUFFER_SIZE];
  uint8_t indexWrite;
  uint8_t indexRead;
  uint8_t count;
  void read(HeartRSensorData *data);
public:
  HeartRSensor();
  void clear();
  bool load(HeartRSensorData *data);
  void flush();
  bool loadAndFlush(HeartRSensorData *data);
};
/* ------------------------------------------- */
#define TEMP_SENSOR_BUFFER_SIZE 10
typedef struct{
  time_t time;
  uint16_t temp;
}TempSensorData;

class TempSensor
{
protected:
  TempSensorData buffer[TEMP_SENSOR_BUFFER_SIZE];
  uint8_t indexWrite;
  uint8_t indexRead;
  uint8_t count;
  void read(TempSensorData *data);
public:
  TempSensor();
  void clear();
  bool load(TempSensorData *data);
  void flush();
  bool loadAndFlush(TempSensorData *data);
};
#endif 
