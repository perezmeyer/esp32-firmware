/*
 * MoSimPa ESP32 firmware
 * Copyright 2020 
 * Lisandro Damián Nicanor Pérez MEyer <perezmeyer@gmail.com>
 * Casanova Alejandro <acasanova.mails@gmail.com>
 */ 
#include "MqttClient.h"
#include "Configuration.h"
#include "printf.h"


MqttClient::MqttClient()
:_pubSubClient(_mqttClient)
{  
}
bool MqttClient::connectWifi(const char *ssid, const char *pass){
  int status = WL_IDLE_STATUS;
  int attempts = 100;  

  printf("Waiting for WiFi...");
  WiFi.begin(ssid, pass);
  while ((status != WL_CONNECTED) && (attempts > 0)) {
    status = WiFi.status();
    delay(500);
    Serial.print(".");
    attempts--;
  } 

  if(status != WL_CONNECTED){
    printfn("Couldn't get a wifi connection");
    return false;
  }
  printfn("Connected to wifi");
  
  return true;
}
bool MqttClient::connectBroker(const char *ip, int port){
  int attempts = 100;

  _id = WiFi.macAddress();  
  _id.replace(":", "");
  _id.toLowerCase();
  printfn("ID: %s", _id.c_str());
   
  printfn("Starting MQTT connection...");
  // if you get a connection, report back via serial:
  _pubSubClient.setServer(ip, port);
  // Attempting MQTT connection...
  while((!_pubSubClient.connected()) && (attempts > 0)){
    if(_pubSubClient.connect("arduinoClient")){
      printfn("connected...");      
    }else {
      printfn("failed, rc = %u try again in 5 seconds", _pubSubClient.state());
      delay(5000); 
    }
    attempts--;
  }

  if(attempts == 0){
    return false;
  }

  return true;
}

bool MqttClient::isConnectedWiFi(){

  if(WiFi.status() == WL_CONNECTED){
    return true;
  }

  return false;
}

bool MqttClient::isConnectedBroker(){

  return _pubSubClient.connected();
}

String MqttClient::getID(){

  return _id;
}
bool MqttClient::publish(const char *topic, const char *message){

  _pubSubClient.publish(topic, message);
}
