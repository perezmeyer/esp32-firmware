/*
 * MoSimPa ESP32 firmware
 * Copyright 2020 
 * Lisandro Damián Nicanor Pérez MEyer <perezmeyer@gmail.com>
 * Casanova Alejandro <acasanova.mails@gmail.com>
 */ 
#include "ConfigurationWebServer.h"
#include "printf.h"
#include "Configuration.h"

// Network credentials.
const char* ssid     = "mosimpa-";
const char* password = "Configurame";

ConfigurationWebServer::ConfigurationWebServer(int port)
: _server(port)
, _configured(false)
{  
}

void ConfigurationWebServer::begin(void){  
  // Connect to Wi-Fi network with SSID and password
  printfn("Setting AP (Access Point)…");
  String ssidWithMac(ssid + WiFi.macAddress());
  printfn("SSID: %s", ssidWithMac.c_str());  
  WiFi.softAP(ssidWithMac.c_str(), password);
  printfn("MAC address: %s", WiFi.macAddress().c_str());
  IPAddress IP = WiFi.softAPIP();
  printfn("AP IP address: %s", IP.toString().c_str());
  
  _server.begin();      
}

bool ConfigurationWebServer::handleClient(void){
  String header;
  _currentClient = _server.available();
  
  if (_currentClient) {
    printfn("Client connected.");
    
    String currentLine = "";
    while (_currentClient.connected()) {
      if (_currentClient.available()) {
        char c = _currentClient.read();
        header += c;
        Serial.write(c);
        
        if (c == '\n') {
          // The last line of an HTTP request is empty. Send the form.
          if (currentLine.length() == 0) {
            _sendWebPage();
            // Break out of the while loop
            break;
          } else {
            int indexIni = currentLine.indexOf("GET /"); 
            if(indexIni != -1){
              if(currentLine.substring(indexIni, indexIni + 6) == "GET /?"){
                indexIni = indexIni + 6;
                int indexEnd = currentLine.indexOf(" ", indexIni);              
                if(_configurationParse(currentLine.substring(indexIni, indexEnd))){
                  printfn("Configuration sucefull");
                  _configured = true;
                }              
              }
            }
            currentLine = "";
          }
        } else if (c != '\r') {
          currentLine += c;
        }
      }
    }
    header = "";
    _currentClient.stop();
    printfn("Client disconnected.\r\n");
  }

  return _configured;
}

void ConfigurationWebServer::end(void){
  _server.close();
  WiFi.enableSTA(true);
}

void ConfigurationWebServer::_sendWebPage(void){
  _currentClient.println("HTTP/1.1 200 OK");
  _currentClient.println("Content-type:text/html");
  _currentClient.println("Connection: close");
  _currentClient.println();
  
  // Display the HTML web page
  _currentClient.println("<!DOCTYPE html><html>");
  _currentClient.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
  _currentClient.println("<link rel=\"icon\" href=\"data:,\">");
  
  _currentClient.println("<h1>MoSimPa sensor configuration</h1>");
  
  _currentClient.println("<h2>Device parameters</h2>");
  _currentClient.println("<form method=\"get\">");
  _currentClient.println("  <fieldset>");
  _currentClient.println("    <legend>WiFi</legend>");
  _currentClient.println("    <label for=\"wifissid\">SSID:</label>");
  _currentClient.println("    <input type=\"text\" id=\"wifissid\" name=\"wifissid\" placeholder=\"Your WiFi SSID (name)\" required autocomplete=\"on\"><br>");
  _currentClient.println("    <label for=\"wifipass\">Password:</label>");
  _currentClient.println("    <input type=\"password\" id=\"wifipass\" name=\"wifipass\" placeholder\"mysecretpassword\" required><br>");
  _currentClient.println("  </fieldset>");
  _currentClient.println("  <fieldset>");
  _currentClient.println("    <legend>MQTT broker</legend>");
  _currentClient.println("    <label for=\"mqttip\">IP:</label>");
  _currentClient.println("    <input type=\"text\" minlength=\"7\" maxlength=\"15\" size=\"15\" pattern=\"\\b(?:(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\.){3}(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\\b\" id=\"mqttip\" name=\"mqttip\" placeholder=\"192.168.1.5\" required autocomplete=\"on\"><br>");
  _currentClient.println("    <label for=\"mqttport\">Port:</label>");
  _currentClient.println("    <input type=\"number\" id=\"mqttport\" name=\"mqttport\" placeholder=\"1883\" required autocomplete=\"on\"><br>");
  _currentClient.println("    <br>");
  _currentClient.println("  <input type=\"submit\" value=\"Submit\">");
  _currentClient.println("</form>");
  
  _currentClient.println("</body></html>");
  
  // The HTTP response ends with another blank line
  _currentClient.println();    
}

bool ConfigurationWebServer::_configurationParse(String uri){
  int indexIni, indexEnd;
  String parameter;

  //wifissid=Network&wifipass=1212&mqttip=192.168.100.10&mqttport=1883
  indexIni = uri.indexOf("=") + 1;     
  indexEnd = uri.indexOf("&", indexIni);
  parameter = uri.substring(indexIni, indexEnd);
  Configuration.setSSID(parameter);

  indexIni = uri.indexOf("=", indexEnd) + 1;   
  indexEnd = uri.indexOf("&", indexIni);
  parameter = uri.substring(indexIni, indexEnd);
  Configuration.setPass(parameter); 

  indexIni = uri.indexOf("=", indexEnd) + 1;     
  indexEnd = uri.indexOf("&", indexIni);
  parameter = uri.substring(indexIni, indexEnd);
  Configuration.setMqttIP(parameter);

  indexIni = uri.indexOf("=", indexEnd)+ 1;     
  parameter = uri.substring(indexIni);
  Configuration.setMqttPort(parameter.toInt());

  Configuration.commit();

  return true;  
}
