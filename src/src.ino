/*
 * MoSimPa ESP32 firmware
 * Copyright 2020 Lisandro Damián Nicanor Pérez MEyer <perezmeyer@gmail.com>
 * License: to be defined, but possibly MIT/Apache 2.0.
 */

/**
 * \todo We are sending the password in clear.
 */
#include "printf.h"
#include "Configuration.h"
#include "ConfigurationWebServer.h"
#include "MqttClient.h"
#include "LocalTime.h"
#include "SensorsHandler.h"
#include <ArduinoJson.h>
/* ------------------------------------------------------------- */
#define PERIOD_SEND_INFO    60  // seconds
#define LED_BUILTIN         2
/* ------------------------------------------------------------- */
ConfigurationWebServer webServer;   ///< web server utilizado para realizar la configuracion del dispositivo (ssid, pass, mqtt ip, mqtt port)
MqttClient mqttClient;              ///< Cliente MQTT que envia los datos al broker
bool configuredDevice = false;      ///< El dispositivo esta configurado? No, iniciamos AP
Ticker tickerSendInfo;              ///< Configuramos un ticker periodico para envio de datos de info del dispositivo (ID, Bateria, time)
Ticker tickerLed;                   ///<
StaticJsonDocument<100> infoJson;   ///< Json usado para el envio de info del dispositivo
SPO2Sensor spO2Sensor;              ///< 
BloodPSensor bloodPSensor;          ///< 
HeartRSensor heartRSensor;          ///< 
TempSensor tempSensor;              ///< 
uint32_t msgId = 0;                 ///< ID de mensaje utilizado en el mensaje a devices/info.
/* ------------------------------------------------------------- */
bool mqttClient_initialize(void);
void mqttClient_connectionStatus(void);
void mqttClient_sendInfo(void);
void sensors_process(void);
/* ------------------------------------------------------------- */
void blinkLed(){
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
}
/**
 * \brief Inicializacion
 */
void setup() {  
  bool serverEnable = false;  // Para debug

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  tickerLed.attach(0.25, blinkLed);
  Serial.begin(115200);
  
  Configuration.begin();
  configuredDevice = Configuration.isSetting();
  // --------------------- DEBUG -------------------------------
  Serial.println("Select mode: [s] = Server; [c] = Client");
  while(true){
    if(Serial.available() > 0){
      char cmd = Serial.read();
      Serial.print("I received: ");
      Serial.println(cmd);
      if(cmd == 's'){
        configuredDevice = false;
      }
      break;
    }
  }
  // ---------------------------------------------------------  
  
  if(configuredDevice == true){
    // Se conectara a wifi y al broker configurado
    printfn("EEPROM configured:");
    printfn("\tssid = %s", Configuration.getSSID());
    printfn("\tpass = %s", Configuration.getPass());
    printfn("\tmqttip = %s", Configuration.getMqttIP());
    printfn("\tmqttport = %u", Configuration.getMqttPort());

    configuredDevice = mqttClient_initialize();
    if(configuredDevice){
      LocalTime_begin();    // Retornara solo si puede conectarse al NTP
      mqttClient_sendInfo();
      tickerSendInfo.attach(PERIOD_SEND_INFO, mqttClient_sendInfo);      
    }
  }else{
    // Inicia modo AP y habilita servidor web
    printfn("EEPROM NO configured");    
    webServer.begin();    
  }  
}
/**
 * \brief bucle infinito
 */
void loop(){
  char message[20];
  static uint8_t count = 0;
  
  if(configuredDevice == false){
    // Procesamiento del servidor web.
    // Responde a las peticiones del cliente y recibe la configuracion
    if(webServer.handleClient() == true){
      configuredDevice = mqttClient_initialize();
    }      
  }else{
    // Controla conexion con el broker.. Si no esta conectado reintenta conexión
    mqttClient_connectionStatus();
    // Lectura de sensores, procesamiento y envio...  
    sensors_process();
  } 
  delay(100);  
}
/* ------------------------------------------------------------- */
/* ------------------------------------------------------------- */
/**
 * \brief Inicia el cliente MQTT. Se conecta al wifi y al broker
 */
bool mqttClient_initialize(void){
  
  if(mqttClient.connectWifi(Configuration.getSSID(), Configuration.getPass())){
    if(mqttClient.connectBroker(Configuration.getMqttIP(), Configuration.getMqttPort())){
      return true;
    }
  }
  return false;
}
/**
 * \brief Controla conexion con el broker.. Si no esta conectado reintenta conexión
 */
void mqttClient_connectionStatus(void){
  
  if(!mqttClient.isConnectedBroker()){
    if(mqttClient.isConnectedWiFi()){
      mqttClient.connectBroker(Configuration.getMqttIP(), Configuration.getMqttPort());
    }else{
      mqttClient_initialize();
    }
  }  
}
/**
 * \brief
 */
void mqttClient_sendInfo(void){
  String message;
  infoJson.clear();

  if(mqttClient.isConnectedBroker()){
    infoJson["WiFiMAC"] = mqttClient.getID();
    infoJson["battmV"] = (uint16_t)random(320, 340);;
    infoJson["time"] = LocalTime_getSeconds(); //LocalTime_getMilliseconds();
    infoJson["msgId"] = msgId;
    serializeJson(infoJson, message);

    // Increase the ID for the next message.
    msgId++;
  
    printfn("%s", message.c_str());
    mqttClient.publish("devices/info", message.c_str());
  }
}
/**
 * \brief
 */
void sensors_process(void){
  SPO2SensorData spO2Data;
  BloodPSensorData bloodPData;
  HeartRSensorData heartRData;
  TempSensorData tempData;
  uint8_t k;

  for(k = 0; k < 5; k++){
    spO2Data.time = LocalTime_getSeconds(); //LocalTime_getMilliseconds();
    spO2Data.spO2 = (uint16_t)random(0, 99);
    spO2Data.R = (uint16_t)random(0, 100);
    spO2Sensor.loadAndFlush(&spO2Data);  
  
    heartRData.time = LocalTime_getSeconds(); //LocalTime_getMilliseconds();
    heartRData.heartR = (uint16_t)random(0, 100)*3.02 + 18;
    if(heartRData.heartR > 95){
      heartRData.HR_AR = true;
    }else{
      heartRData.HR_AR = false;
    }
    heartRSensor.loadAndFlush(&heartRData);  
    delay(1000); 
  }

  bloodPData.time = LocalTime_getSeconds(); //LocalTime_getMilliseconds();
  bloodPData.sys = (uint16_t)random(0, 100);
  bloodPData.dia = (uint16_t)random(0, 100);
  bloodPSensor.loadAndFlush(&bloodPData);  

  tempData.time = LocalTime_getSeconds(); //LocalTime_getMilliseconds();
  tempData.temp = (uint16_t)random(32, 40);
  tempSensor.loadAndFlush(&tempData); 
}
