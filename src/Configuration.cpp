/*
 * MoSimPa ESP32 firmware
 * Copyright 2020 
 * Lisandro Damián Nicanor Pérez MEyer <perezmeyer@gmail.com>
 * Casanova Alejandro <acasanova.mails@gmail.com>
 */ 
#include "Configuration.h"
#include "printf.h"
#include <EEPROM.h>

ConfigurationClass::ConfigurationClass(){
}

void ConfigurationClass::begin(){
  parameters.flag = 0xFF;
  EEPROM.begin(100);
}

bool ConfigurationClass::isSetting(void){
  Parameters tmp;
  
  EEPROM.get(0, tmp);
  if(tmp.flag == 0xA5){
    parameters = tmp;
    return true;
  }

  return false;
}

char *ConfigurationClass::getSSID(void){
  if(parameters.flag == 0xA5){
    return &parameters.wifissid[0];
  }

  return NULL;
}
char *ConfigurationClass::getPass(void){
  if(parameters.flag == 0xA5){
    return &parameters.wifipass[0];
  }

  return NULL;
}
char *ConfigurationClass::getMqttIP(void){
  if(parameters.flag == 0xA5){
    return &parameters.mqttip[0];
  }

  return NULL;
}
int ConfigurationClass::getMqttPort(void){
  if(parameters.flag == 0xA5){
    return parameters.mqttport;
  }

  return 0;
}
void ConfigurationClass::setSSID(String ssid){
  
  ssid.toCharArray(parameters.wifissid, WIFI_SSID_SIZE);
}
void ConfigurationClass::setPass(String pass){
  
  pass.toCharArray(parameters.wifipass, WIFI_PASS_SIZE);
}
void ConfigurationClass::setMqttIP(String ip){
  
  ip.toCharArray(parameters.mqttip, MQTT_BROKER_IP_SIZE);
}
void ConfigurationClass::setMqttPort(int port){

  parameters.mqttport = port;
}

void ConfigurationClass::commit(void){
  parameters.flag = 0xA5;
  EEPROM.put(0, parameters);
  EEPROM.commit();  

  printfn("Configuration commit: flag = 0x%02X", parameters.flag);
  printfn("Configuration commit: ssid = %s", parameters.wifissid);
  printfn("Configuration commit: pass = %s", parameters.wifipass);
  printfn("Configuration commit: mqttip = %s", parameters.mqttip);
  printfn("Configuration commit: mqttport = %u", parameters.mqttport);
}

ConfigurationClass Configuration;
