/*
 * MoSimPa ESP32 firmware
 * Copyright 2020 
 * Lisandro Damián Nicanor Pérez MEyer <perezmeyer@gmail.com>
 * Casanova Alejandro <acasanova.mails@gmail.com>
 */ 

#ifndef LOCAL_TIME_H
#define LOCAL_TIME_H
#include <inttypes.h>
#include <Arduino.h>
#include "time.h"
#include <Ticker.h>

#define USE_EPOCH 1

#define LocalTime_getMilliseconds() millis()

void LocalTime_begin();
time_t LocalTime_getSeconds(void);
time_t LocalTime_getEpoch(void);

#endif
