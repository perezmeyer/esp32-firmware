/*
 * MoSimPa ESP32 firmware
 * Copyright 2020 
 * Lisandro Damián Nicanor Pérez MEyer <perezmeyer@gmail.com>
 * Casanova Alejandro <acasanova.mails@gmail.com>
 */ 
#ifndef CONFIGURATION_WEB_SERVER_H
#define CONFIGURATION_WEB_SERVER_H
#include <inttypes.h>
#include <Arduino.h>
#include <WiFi.h>

class ConfigurationWebServer{
protected:
  WiFiServer _server;
  WiFiClient _currentClient;
  bool _configured;

  void _sendWebPage(void);
  bool _configurationParse(String uri);
public:
  ConfigurationWebServer(int port = 80);
  void begin(void);
  bool handleClient(void);
  void end(void);
};


#endif
