#include "SensorsHandler.h"
#include "MqttClient.h"
#include <ArduinoJson.h>

const size_t docJsonSize = JSON_ARRAY_SIZE(1) + JSON_OBJECT_SIZE(1) + JSON_OBJECT_SIZE(3);

extern MqttClient mqttClient;
/* ------------------------------------------------- */
/*                  SPO2 SENSOR
/* ------------------------------------------------- */
SPO2Sensor::SPO2Sensor(){
  indexWrite = 0;
  indexRead = 0;
  count = 0;
}
void SPO2Sensor::clear(){
  
  indexWrite = 0;
  indexRead = 0;
  count = 0;
}
bool SPO2Sensor::load(SPO2SensorData *data){ 

  if(count == SPO2_SENSOR_BUFFER_SIZE){
    return false;
  }
  buffer[indexWrite] = *data;
  if(++indexWrite == SPO2_SENSOR_BUFFER_SIZE){
    indexWrite = 0;
  }
  count++;
  return true;
}
// {
//  "spo2": [
//    { "time": 1212144844, "SpO2": 0, "R" : 0 }
//  ]
// }
void SPO2Sensor::flush(){
  StaticJsonDocument<docJsonSize> doc;
  SPO2SensorData data;
  String message;

  if(count == 0){
    return;
  }
  // Generacion JSON  
  while(count > 0){
    read(&data);    
    doc.clear();
    JsonArray array = doc.createNestedArray("spo2");
    JsonObject array_0 = array.createNestedObject();
    array_0["time"] = data.time;
    array_0["SpO2"] = data.spO2;
    array_0["R"] = data.R;   
    serializeJson(doc, message);

    String topic("reads/" + mqttClient.getID());
    mqttClient.publish(topic.c_str(), message.c_str());
  }
}
bool SPO2Sensor::loadAndFlush(SPO2SensorData *data){

  load(data);
  flush();
}
void SPO2Sensor::read(SPO2SensorData *data){
  if(count == 0){
    return;
  }
  *data = buffer[indexRead];
  if(++indexRead == SPO2_SENSOR_BUFFER_SIZE){
    indexRead = 0;
  }
  count--;  
}
/* ------------------------------------------------- */
/*                BLOODP SENSOR
/* ------------------------------------------------- */
BloodPSensor::BloodPSensor(){
  indexWrite = 0;
  indexRead = 0;
  count = 0;
}
void BloodPSensor::clear(){
  
  indexWrite = 0;
  indexRead = 0;
  count = 0;
}
bool BloodPSensor::load(BloodPSensorData *data){ 

  if(count == BLOODP_SENSOR_BUFFER_SIZE){
    return false;
  }
  buffer[indexWrite] = *data;
  if(++indexWrite == BLOODP_SENSOR_BUFFER_SIZE){
    indexWrite = 0;
  }
  count++;
  return true;
}
// {
//  "bloodP": [
//    { "time": 1212144844, "sys": 0, "dia" : 0 }
//  ]
// }
void BloodPSensor::flush(){
  StaticJsonDocument<docJsonSize> doc;
  BloodPSensorData data;
  String message;

  if(count == 0){
    return;
  }
  // Generacion JSON  
  while(count > 0){
    read(&data);    
    doc.clear();
    JsonArray array = doc.createNestedArray("bloodP");
    JsonObject array_0 = array.createNestedObject();
    array_0["time"] = data.time;
    array_0["sys"] = data.sys;
    array_0["dia"] = data.dia;   
    serializeJson(doc, message);
    
    String topic("reads/" + mqttClient.getID());
    mqttClient.publish(topic.c_str(), message.c_str());
  }
}
bool BloodPSensor::loadAndFlush(BloodPSensorData *data){

  load(data);
  flush();
}
void BloodPSensor::read(BloodPSensorData *data){
  if(count == 0){
    return;
  }
  *data = buffer[indexRead];
  if(++indexRead == BLOODP_SENSOR_BUFFER_SIZE){
    indexRead = 0;
  }
  count--;  
}
/* ------------------------------------------------- */
/*                HEARTR SENSOR
/* ------------------------------------------------- */
HeartRSensor::HeartRSensor(){
  indexWrite = 0;
  indexRead = 0;
  count = 0;
}
void HeartRSensor::clear(){
  
  indexWrite = 0;
  indexRead = 0;
  count = 0;
}
bool HeartRSensor::load(HeartRSensorData *data){ 

  if(count == HEARTR_SENSOR_BUFFER_SIZE){
    return false;
  }
  buffer[indexWrite] = *data;
  if(++indexWrite == HEARTR_SENSOR_BUFFER_SIZE){
    indexWrite = 0;
  }
  count++;
  return true;
}
// {
//  "heartR": [
//    { "time": 1212144844, "heartR": 0, "HR_AR" : 0 }
//  ]
// }
void HeartRSensor::flush(){
  StaticJsonDocument<docJsonSize> doc;
  HeartRSensorData data;
  String message;

  if(count == 0){
    return;
  }
  // Generacion JSON  
  while(count > 0){
    read(&data);    
    doc.clear();
    JsonArray array = doc.createNestedArray("heartR");
    JsonObject array_0 = array.createNestedObject();
    array_0["time"] = data.time;
    array_0["heartR"] = data.heartR;
    if(data.HR_AR){
      array_0["HR_AR"] = "true"; 
    }else{
      array_0["HR_AR"] = "false";  
    } 
    serializeJson(doc, message);

    String topic("reads/" + mqttClient.getID());
    mqttClient.publish(topic.c_str(), message.c_str());
  }
}
bool HeartRSensor::loadAndFlush(HeartRSensorData *data){

  load(data);
  flush();
}
void HeartRSensor::read(HeartRSensorData *data){
  if(count == 0){
    return;
  }
  *data = buffer[indexRead];
  if(++indexRead == HEARTR_SENSOR_BUFFER_SIZE){
    indexRead = 0;
  }
  count--;  
}
/* ------------------------------------------------- */
/*                TEMP SENSOR
/* ------------------------------------------------- */
TempSensor::TempSensor(){
  indexWrite = 0;
  indexRead = 0;
  count = 0;
}
void TempSensor::clear(){
  
  indexWrite = 0;
  indexRead = 0;
  count = 0;
}
bool TempSensor::load(TempSensorData *data){ 

  if(count == TEMP_SENSOR_BUFFER_SIZE){
    return false;
  }
  buffer[indexWrite] = *data;
  if(++indexWrite == TEMP_SENSOR_BUFFER_SIZE){
    indexWrite = 0;
  }
  count++;
  return true;
}
// {
//  "bodyT": [
//    { "time": 1212144844, "temp": 0 }
//  ]
// }
void TempSensor::flush(){
  StaticJsonDocument<docJsonSize> doc;
  TempSensorData data;
  String message;

  if(count == 0){
    return;
  }
  // Generacion JSON  
  while(count > 0){
    read(&data);    
    doc.clear();
    JsonArray array = doc.createNestedArray("bodyT");
    JsonObject array_0 = array.createNestedObject();
    array_0["time"] = data.time;
    array_0["temp"] = data.temp;
    serializeJson(doc, message);

    String topic("reads/" + mqttClient.getID());
    mqttClient.publish(topic.c_str(), message.c_str());
  }
}
bool TempSensor::loadAndFlush(TempSensorData *data){

  load(data);
  flush();
}
void TempSensor::read(TempSensorData *data){
  if(count == 0){
    return;
  }
  *data = buffer[indexRead];
  if(++indexRead == TEMP_SENSOR_BUFFER_SIZE){
    indexRead = 0;
  }
  count--;  
}
