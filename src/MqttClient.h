/*
 * MoSimPa ESP32 firmware
 * Copyright 2020 
 * Lisandro Damián Nicanor Pérez MEyer <perezmeyer@gmail.com>
 * Casanova Alejandro <acasanova.mails@gmail.com>
 */ 
#ifndef MQTT_CLIENT_H
#define MQTT_CLIENT_H
#include <inttypes.h>
#include <Arduino.h>
#include <WiFi.h>
#include <PubSubClient.h> // https://github.com/knolleary/pubsubclient

class MqttClient{
protected:
  WiFiClient _mqttClient;
  PubSubClient _pubSubClient;
  String _id;
public:
  MqttClient();
  bool connectWifi(const char *ssid, const char *pass);
  bool connectBroker(const char *ip, int port);
  bool isConnectedWiFi();
  bool isConnectedBroker();
  String getID();
  bool publish(const char *topic, const char *message); 
};

#endif
